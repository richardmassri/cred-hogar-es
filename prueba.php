<?php
require 'leadsolution.class.php';
//transformar los input por post en un satring
$_POST = json_decode(file_get_contents('php://input'), true);


//capturar por post Los elemntos
$sexo=isset($_POST['gender'])?$_POST['gender']:'';
$nombre=isset($_POST['firstname'])?$_POST['firstname']:'';
$apellido=isset($_POST['lastname'])?$_POST['lastname']:'';
$email=isset($_POST['email'])?$_POST['email']:'';
$email_repetir=isset($_POST['email_repetir'])?$_POST['email_repetir']:'';
$dia=isset($_POST['day'])?trim($_POST['day']):'';
$mes=isset($_POST['month'])?trim($_POST['month']):'';
$annio=isset($_POST['year'])?trim($_POST['year']):'';
$celular=isset($_POST['mobile_phone'])?$_POST['mobile_phone']:'';
$telefono_fijo=isset($_POST['phone'])?$_POST['phone']:'';
$zipcode=isset($_POST['zipcode'])?$_POST['zipcode']:'';
$agree=isset($_POST['agree'])?$_POST['agree']:'';


//Validaciones de los campos
if($nombre==''){
	$error['name']="El nombre no debe ser vacio";
}else if(preg_match("/^[a-zA-Z ]*$/",$nombre)==false){
	$error['name']="El nombre debe contener solo letras";
}elseif(strlen($nombre)<=3){
	$error['name']="El nombre debe contener al menos 4 letras";
}

if($apellido==''){
	$error['apellido']="El apellido no debe ser vacio";
}elseif(preg_match("/^[a-zA-Z ]*$/",$apellido)==false){
	$error['apellido']="El apellido debe contener solo letras";
}elseif(strlen($apellido)<=3){
	$error['apellido']="El apellido debe contener al menos 4 letras";
}


if($email==''){
	$error['email']="El email no debe ser vacio";
}elseif(preg_match("/^([a-zA-Z0-9_\.\-]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/",$email)==false){
	$error['email']="Debe ingresar un correo valido";
}



if($email_repetir==''){
	$error['email_repetir']="El email no debe ser vacio";
}elseif(preg_match("/^([a-zA-Z0-9_\.\-]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/",$email_repetir)==false){
	$error['email_repetir']="Debe ingresar un correo valido";
}

if($email_repetir!==$email){
	$error['email_repetir']="El email no coincide";
}

if($dia==''){
	$error['dia']="El dia no debe ser vacio";
}

if($mes==''){
	$error['mes']="El mes no debe ser vacio";
}

if($annio==''){
	$error['annio']="El año no debe ser vacio";
}

if($celular==''){
	$error['celular']="El telefono célular no debe ser vacio";
}elseif (preg_match("/^((\+?34([ \t|\-])?)?[6]((\d{1}([ \t|\-])?[0-9]{3})|(\d{2}([ \t|\-])?[0-9]{2}))([ \t|\-])?[0-9]{2}([ \t|\-])?[0-9]{2})$/",$celular)==false){
	$error['celular']="El telefono célular no coincide";
	# code...
}

if($telefono_fijo==''){
	$error['telefono_fijo']="El telefono fijo no debe ser vacio";
}elseif (preg_match("/^((\+?34([ \t|\-])?)?[9]((\d{1}([ \t|\-])?[0-9]{3})|(\d{2}([ \t|\-])?[0-9]{2}))([ \t|\-])?[0-9]{2}([ \t|\-])?[0-9]{2})$/",$telefono_fijo)==false){
	$error['telefono_fijo']="El telefono fijo no coincide";
	# code...
}

if($agree==''){
	$error['agree']="Debe declarar que es mayor de edad";
}

if($sexo==''){
	$error['sexo']="El sexo no debe ser vacio";
}

if($zipcode==''){
	$error['zipcode']="El codigo postal no debe ser vacio";
}elseif(preg_match("/^[00-52]{2}[0-9]{3}$/",$celular)==false){

}





$succes['exito']='';
if(!empty($error)){
	echo json_encode(['error'=>$error,'succes'=>$succes]);
	$error['error']=$error;
}else{
	$res = Leadsolution::handle('http://espanha.leadsolution.com.br/leads');
	$succes['exito']='Mensaje enviado de forma Exitosa. Enviamos un correo con la información requerida';
	if($res->isOk()):
	$res->pixels();
	$res->tags();
	else: $res->log(); 
	endif;
}

?>