//funcion para evitar colapso de variables o lo que se llama iffe
(function(){
//Modulo de angular
	angular.module('miModuloIndex',['ngMask']).
	controller('miControllerIndex', ['$scope','$timeout','$http', function($scope,$timeout,$http){
	$scope.errorRepetirEmail=false;
	$scope.mensajeExito=false;
     //console.log($scope.aceptar);
	var json_dias =
	{
		'dia':['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']
	}
	var json_mes =
	{
		'mes':['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
	}
	var json_anio =
	{
		'anio':['1917','1918','1919','1920','1921','1922','1923','1924','1925','1926','1927','1928','1929','1930','1931','1932','1933','1934','1935','1936','1937','1938','1939','1940','1941','1942','1943','1944','1945','1946','1947','1948','1949','1950','1951','1952','1953','1954','1955','1956','1957','1958','1959','1960','1961','1962','1963','1964','1965','1966','1967','1968','1969','1970','1971','1972','1973','1974','1975','1976','1977','1978','1979','1980','1981','1982','1983','1984','1985','1986','1987','1988','1989','1990','1991','1992','1993','1994','1995','1996','1997']
	}


	$scope.dias=json_dias['dia'];
      $scope.meses=json_mes['mes'];
      $scope.anios=json_anio['anio'];

	$scope.$watchGroup(["user.email","user.email_repetir"],function(nuevo,anterios){
	//console.log(nuevo);
	if (!nuevo[0] || !nuevo[1])return;
		if(nuevo[0] !== nuevo[1]){
			$scope.errorRepetirEmail="El correo no coincide";
		}else{
			$scope.errorRepetirEmail=false;
		}
	});
	$scope.$watch("user.firstname",function(nuevo,anterios){
	   if (!nuevo) return;
		if(!/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
			$scope.errorName="El nombre debe contener solo letras";
		}else if(nuevo.length<=3){
			$scope.errorName="El nombre debe contener al menos 4 letras";
		}else{
			$scope.errorName=false;
			//return false;
			//return false;
		}
	});

		$scope.$watch("user.zipcode",function(nuevo,anterios){
	   if (!nuevo) return;
		if(!/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/.test(nuevo)){
			$scope.errorZipcode="El codigo postal debe contener solo numeros y coincidir con su estado";
			//return false;
		} else {
			$scope.errorZipcode=false;
			//return false;
			//return false;
		}
		});
		$scope.$watch("user.email",function(nuevo,anterios){
		 if (!nuevo) return;
		if(!/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/.test(nuevo)){
			$scope.errorEmail="Por favor ingresar un correo valido";
			return false;
			//return false;
		} else {
			$scope.errorEmail=false;
			//return false;
			//return false;
		}

		});

		$scope.$watch("user.email_repetir",function(nuevo,anterios){
		 if (!nuevo) return;
		 console.log(nuevo);
		if(!/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/.test(nuevo)){
			$scope.errorRepetirEmail="Por favor ingresar un correo valido";
			//return false;
		} else {
			$scope.errorRepetirEmail=false;
			//return false;
			//return false;
		}

		});

	$scope.$watch("user.lastname",function(nuevo,anterios){
	 if (!nuevo) return;
		if(!/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
			$scope.errorApellido="El apellido debe contener solo letras";
			//return false;
		}else if(nuevo.length<=3){
		$scope.errorApellido="El apellido debe contener al menos 4 letras";
		} else {
			$scope.errorApellido=false;
			//return false;
			//return false;
		}
	});

	 $scope.$watch("user.mobile_phone",function(nuevo,anterios){
	 if (!nuevo) return;
		if(!/^((\+?34([ \t|\-])?)?[6]((\d{1}([ \t|\-])?[0-9]{3})|(\d{2}([ \t|\-])?[0-9]{2}))([ \t|\-])?[0-9]{2}([ \t|\-])?[0-9]{2})$/.test(nuevo)){
			$scope.errorCelular="El telefono celular no coincide";
			//return false;
		} else {
			$scope.errorCelular=false;
			//return false;
			//return false;
		}
	});

	 $scope.$watch("user.agree",function(nuevo,anterios){
	 if (!nuevo) return;
		if(nuevo){
			//console.log(nuevo);
			$scope.errorAceptar=false;
			//return false;
		} else {
			$scope.errorAceptar="Debe declarar que es mayor de edad";
			//return false;
			//return false;
		}
	});

	 $scope.$watch("user.phone",function(nuevo,anterios){
	 if (!nuevo) return;
		if(!/^((\+?34([ \t|\-])?)?[9]((\d{1}([ \t|\-])?[0-9]{3})|(\d{2}([ \t|\-])?[0-9]{2}))([ \t|\-])?[0-9]{2}([ \t|\-])?[0-9]{2})$/.test(nuevo)){
			$scope.erroTelefonoFijo="El telefono fijo no coincide";
			//return false;
		} else {
			$scope.erroTelefonoFijo=false;
			//return false;
			//return false;
		}
	});
	
	$scope.valorLoading=true;
	$scope.user = {
	agree:false
	}
		 //console.log($scope.user.firstname);

	$scope.$watch("user.day",function(nuevo,anterios){
	 if (!nuevo) return;
		if(nuevo.length){
			$scope.errorDia=false;
			//return false;
		} else {
			$scope.errorDia=true;
			//console.log("aquiii2222");
			//return false;
			//return false;
		}
	});

	$scope.$watch("user.month",function(nuevo,anterios){
	 if (!nuevo) return;
		if(nuevo.length){
			$scope.errorMes=false;
			//return false;
		} else {
			$scope.errorMes=true;
			//console.log("aquiii2222");
			//return false;
			//return false;
		}
	});

	$scope.$watch("user.gender",function(nuevo,anterios){
	 if (!nuevo) return;
	 console.log(nuevo);
		if(nuevo){
			$scope.errorSexo=false;
			//return false;
		} else {
			$scope.errorSexo=true;
			//console.log("aquiii2222");
			//return false;
			//return false;
		}
	});

	$scope.$watch("user.year",function(nuevo,anterios){
	 if (!nuevo) return;
		if(nuevo.length){
			$scope.errorAnnio=false;
			//return false;
		} else {
			$scope.errorAnnio=true;
			//console.log("aquiii2222");
			//return false;
			//return false;
		}
	});
	//funcion que actualiza los modelos de los vehiculos
	
	//var postData = 'myData='+JSON.stringify(formData);
	$scope.funcionValidator=function(){
		$timeout(function(){
		$scope.valorLoading=false;
		},1000);
		angular.element('#btn-consultar')
		.html('<i class="fa fa-spinner fa-spin fa-lg fa-fw"></i> Consultado...')
		.prop('disabled', true);
	       $http({
	    	method : 'POST',
	    	dataType: 'json',
	    	url:"prueba.php",
	    	data:$scope.user,
	    	headers : {'Content-Type': 'application/x-www-form-urlencoded'},
             cache: false
	    }).success(function(data){
	    	if(data.error){
		    	$scope.errorSexo = data.error.sexo;
		    	$scope.errorName = data.error.name;
		    	$scope.errorApellido = data.error.apellido;
		    	$scope.errorEmail = data.error.email;
		    	$scope.errorRepetirEmail = data.error.email_repetir;
		    	$scope.errorDia = data.error.dia;
		    	$scope.errorMes = data.error.mes;
		    	$scope.errorAnnio = data.error.annio;
		    	$scope.errorCelular = data.error.celular;
		    	$scope.erroTelefonoFijo = data.error.telefono_fijo;
		    	$scope.errorZipcode = data.error.zipcode;
		    	$scope.errorAceptar = data.error.agree;
		    	
            }else if(data.succes=!''){
            	$scope.mensajeExito = data.succes;
            }
        }).error(function(error){
        console.log(error);
        });
	}
}]);
})();